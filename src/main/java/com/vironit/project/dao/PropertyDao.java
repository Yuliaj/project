package com.vironit.project.dao;

import com.vironit.project.domain.Property;

import java.util.List;

public interface PropertyDao {
    List<Property> findAll();

    List<Property> search(String searchParam);

    List<Property> findByUserId(Long userId);

    Property findOne(Long propertyId);

    Property save(Property property);

    Property update(Property property);

    int delete(Long propertyId);
}
