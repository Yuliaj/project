package com.vironit.project.dao.jdbcTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.vironit.project.dao.UserDao;
import com.vironit.project.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository("userRepositoryJdbcTemplate")
public class UserRepository implements UserDao {
    private static final String ID = "id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String CREATED = "created";
    private static final String CHANGED = "changed";
    private static final String IS_BLOCKED = "is_blocked";
    private static final String IS_DELETED = "is_deleted";
    private static final String PHOTO = "photo";
    private static final String EMAIL = "email";

    private final JdbcTemplate jdbcTemplate;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public UserRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private User userRowMapper(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(ID));
        user.setFirstName(resultSet.getString(FIRST_NAME));
        user.setLastName(resultSet.getString(LAST_NAME));
        user.setLogin(resultSet.getString(LOGIN));
        user.setPassword(resultSet.getString(PASSWORD));
        user.setCreated(resultSet.getTimestamp(CREATED));
        user.setChanged(resultSet.getTimestamp(CHANGED));
        user.setIsBlocked(resultSet.getBoolean(IS_BLOCKED));
        user.setIsDeleted(resultSet.getBoolean(IS_DELETED));
        user.setPhoto(resultSet.getString(PHOTO));
        user.setEmail(resultSet.getString(EMAIL));
        return user;
    }

    @Override
    public List<User> findAll() {
        final String findAllQuery = "select * from m_users order by id desc";
        return jdbcTemplate.query(findAllQuery, this::userRowMapper);
    }

    @Override
    public List<User> search(String searchParam) {
        final String searchQuery = "select * from m_users where login = :searchParam order by id desc";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("searchParam", searchParam);
        return namedParameterJdbcTemplate.query(searchQuery, namedParameters, this::userRowMapper);

    }

    @Override
    public Optional<User> findById(Long userId) {
        return Optional.ofNullable(findOne(userId));
    }

    @Override
    public User findOne(Long userId) {
        final String findOneQuery = "select * from m_users where id = :userId order by id desc\n";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("userId", userId);
        return namedParameterJdbcTemplate.queryForObject(findOneQuery, namedParameters, this::userRowMapper);
    }

    @Override
    public User save(User user) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(user);
        final String insertQuery ="INSERT INTO m_users (first_name, last_name, login, password, email, created, changed, is_blocked, is_deleted)"
                + " VALUES ( :firstName, :lastName, :login, :password, :email, :created, :changed, :isBlocked, :isDeleted)";
        namedParameterJdbcTemplate.update(insertQuery, namedParameters);
        final String getLastId = "SELECT currval('m_users_id_seq') as last_insert_id;";
        Long lastUserId = jdbcTemplate.queryForObject(getLastId, Long.class);
        return findOne(lastUserId);
    }

    @Override
    public User update(User user) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(user);
        final String updateQuery = "update m_users set first_name = :firstName where id = :id;";
        namedParameterJdbcTemplate.update(updateQuery, namedParameters);
        return findOne(user.getId());
    }

    @Override
    public int delete(Long userId) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("userId", userId);
        namedParameters.addValue("isDeleted", true);
        final String deleteQuery = "update m_users set is_deleted = :isDeleted where id = :userId";
        return namedParameterJdbcTemplate.update(deleteQuery, namedParameters);
    }

    @Override
    public Optional<User> findByLogin(String username) {
        try {
            final String findByLogin = "select * from m_users where login = :username order by id desc\n";
            MapSqlParameterSource namedParams = new MapSqlParameterSource();
            namedParams.addValue("username", username);
            return Optional.of(namedParameterJdbcTemplate.queryForObject(findByLogin,namedParams, this::userRowMapper));
        } catch (DataAccessException ex){
            return Optional.empty();
        }
    }

    @Override
    public User blockUser(Long userId) {
        final String blockUser = "update m_users set is_blocked = true where id = :userId";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("userId", userId);
        namedParameterJdbcTemplate.update(blockUser, namedParameters);
        return findOne(userId);
    }

}
