package com.vironit.project.dao.jdbcTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.vironit.project.dao.RoleDao;
import com.vironit.project.domain.Role;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository("roleRepositoryJdbcTemplate")
public class RoleRepository implements RoleDao {
    private static final String ID = "id";
    private static final String ROLE_NAME = "role_name";
    private static final String USER_ID = "user_id";
    private static final String IS_DELETED = "is_deleted";

    private final JdbcTemplate jdbcTemplate;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public RoleRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private Role roleRowMapper(ResultSet resultSet, int i) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getLong(ID));
        role.setRoleName(resultSet.getString(ROLE_NAME));
        role.setUserId(resultSet.getLong(USER_ID));
        role.setIsDeleted(resultSet.getBoolean(IS_DELETED));
        return role;
    }

    @Override
    public List<Role> findAll() {
        final String findAllQuery = "select * from m_roles order by id desc";
        return jdbcTemplate.query(findAllQuery, this::roleRowMapper);
    }

    @Override
    public List<Role> search(String searchParam) {
        final String searchQuery = "select * from m_roles where role_name = :searchParam order by id desc";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("searchParam", searchParam);
        return namedParameterJdbcTemplate.query(searchQuery, namedParameters, this::roleRowMapper);
    }

    @Override
    public List<Role> findByUserId(Long userId) {
        final String findByUserIdQuery = "select * from m_roles where user_id = :userId order by id desc";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();;
        mapSqlParameterSource.addValue("userId", userId);
        return namedParameterJdbcTemplate.query(findByUserIdQuery, mapSqlParameterSource, this::roleRowMapper);
    }

    @Override
    public Role findOne(Long roleId) {
        final String findOneQuery = "select * from m_roles where id = :roleId order by id desc\n";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("roleId", roleId);
        return namedParameterJdbcTemplate.queryForObject(findOneQuery, namedParameters, this::roleRowMapper);
    }

    @Override
    public Role save(Role role) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(role);
        final String insertQuery ="INSERT INTO m_roles (role_name, user_id)"
                + " VALUES ( :roleName, :userId)";
        final String getLastId = "SELECT currval('m_roles_id_seq') as last_insert_id;";
        namedParameterJdbcTemplate.update(insertQuery, namedParameters);
        Long lastRoleId = jdbcTemplate.queryForObject(getLastId, Long.class);
        return findOne(lastRoleId);
    }

    @Override
    public Role update(Role role) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(role);
        final String updateQuery = "update m_roles set role_name = :roleName where id = :id;";
        namedParameterJdbcTemplate.update(updateQuery, namedParameters);
        return findOne(role.getId());
    }

    @Override
    public int delete(Long roleId) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("roleId", roleId);
        namedParameters.addValue("isDeleted", true);
        final String deleteQuery = "update m_roles set is_deleted = :isDeleted where id = :roleId";
        return namedParameterJdbcTemplate.update(deleteQuery, namedParameters);

    }
}
