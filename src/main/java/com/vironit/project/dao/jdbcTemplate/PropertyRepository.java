package com.vironit.project.dao.jdbcTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.vironit.project.dao.PropertyDao;
import com.vironit.project.domain.Property;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository("propertyRepositoryJdbcTemplate")
public class PropertyRepository implements PropertyDao {
    private static final String ID = "id";
    private static final String USER_ID = "user_id";
    private static final String DESCRIPTION = "description";
    private static final String SIZE = "size";
    private static final String PRICE = "price";
    private static final String CREATED = "created";
    private static final String CHANGED = "changed";
    private static final String IS_DELETED = "is_deleted";
    private static final String PHOTO = "photo";

    private final JdbcTemplate jdbcTemplate;

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public PropertyRepository(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private Property propertyRowMapper(ResultSet resultSet, int i) throws SQLException {
        Property property = new Property();
        property.setId(resultSet.getLong(ID));
        property.setUserId(resultSet.getLong(USER_ID));
        property.setDescription(resultSet.getString(DESCRIPTION));
        property.setSize(resultSet.getInt(SIZE));
        property.setPrice(resultSet.getInt(PRICE));
        property.setCreated(resultSet.getTimestamp(CREATED));
        property.setChanged(resultSet.getTimestamp(CHANGED));
        property.setIsDeleted(resultSet.getBoolean(IS_DELETED));
        property.setPhoto(resultSet.getString(PHOTO));
        return property;
    }

    @Override
    public List<Property> findAll() {
        final String findAllQuery = "select * from m_properties order by id desc";
        return jdbcTemplate.query(findAllQuery, this::propertyRowMapper);
    }

    @Override
    public List<Property> search(String searchParam) {
        final String searchQuery = "select * from m_properties where description = :searchParam order by id desc";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("searchParam", searchParam);
        return namedParameterJdbcTemplate.query(searchQuery, namedParameters, this::propertyRowMapper);
    }

    @Override
    public List<Property> findByUserId(Long userId) {
        final String findByUserIdQuery = "select * from m_properties where user_id = :userId order by id desc";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();;
        mapSqlParameterSource.addValue("userId", userId);
        return namedParameterJdbcTemplate.query(findByUserIdQuery, mapSqlParameterSource, this::propertyRowMapper);
    }

    @Override
    public Property findOne(Long propertyId) {
        final String findOneQuery = "select * from m_properties where id = :propertyId order by id desc\n";
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("propertyId", propertyId);
        return namedParameterJdbcTemplate.queryForObject(findOneQuery, namedParameters, this::propertyRowMapper);
    }

    @Override
    public Property save(Property property) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(property);
        final String insertQuery ="INSERT INTO m_properties (user_id, description, size, price, created, changed, is_deleted, photo)"
                + " VALUES (:userId, :description, :size, :price, :created, :changed, :isDeleted, :photo)";
        final String getLastId = "SELECT currval('m_properties_id_seq') as last_insert_id;";
        namedParameterJdbcTemplate.update(insertQuery, namedParameters);
        Long lastRoleId = jdbcTemplate.queryForObject(getLastId, Long.class);
        return findOne(lastRoleId);
    }

    @Override
    public Property update(Property property) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(property);
        final String updateQuery = "update m_properties set description = :description where id = :id;";
        namedParameterJdbcTemplate.update(updateQuery, namedParameters);
        return findOne(property.getId());
    }

    @Override
    public int delete(Long propertyId) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("propertyId", propertyId);
        namedParameters.addValue("isDeleted", true);
        final String deleteQuery = "update m_properties set is_deleted = :isDeleted where id = :propertyId";
        return namedParameterJdbcTemplate.update(deleteQuery, namedParameters);

    }
}
