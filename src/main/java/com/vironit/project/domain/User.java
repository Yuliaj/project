package com.vironit.project.domain;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class User {
    private Long id;

    private String firstName;

    private String lastName;

    private String login;

    private String password;

    private Timestamp created;

    private Timestamp changed;

    private Boolean isBlocked;

    private Boolean isDeleted;

    private String photo;

    private String email;

}
