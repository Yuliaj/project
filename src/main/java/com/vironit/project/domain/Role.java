package com.vironit.project.domain;

import lombok.Data;

@Data
public class Role {
    private Long id;

    private String roleName;

    private Long userId;

    private Boolean isDeleted;

}
