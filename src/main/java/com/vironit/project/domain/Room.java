package com.vironit.project.domain;

import lombok.Data;

@Data
public class Room {
    private Long id;

    private Integer numberOfRooms;

    private Integer propertyId;
}
