package com.vironit.project.domain;

import lombok.Data;

@Data
public class OwnershipType {
    private Long id;

    private String type;

    private Long propertyId;
}
