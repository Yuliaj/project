package com.vironit.project.domain;

import lombok.Data;

@Data
public class PropertyType {
    private Long id;

    private String type;

    private Long propertyId;

}
