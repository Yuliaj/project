package com.vironit.project.domain;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Property {
    private Long id;

    private Long userId;

    private String description;

    private Integer size;

    private Integer price;

    private Timestamp created;

    private Timestamp changed;

    private Boolean isDeleted;

    private String photo;
}
