package com.vironit.project.domain;

import lombok.Data;

@Data
public class Location {
    private Long id;

    private String location;
}
